#pragma once
#include "Map.h"
#include "Maintenance.h"

class Scheduler
{
private:
	Map Trackmap;
	int IntervalCount;
	int CurrentMin;
	std::list<Maintenance>;
public:
	Scheduler();
	Map ReadFiles(std::string);
	int DispMainMenu();
	void MaintenanceCheck();
	void RunSim();
	void Optimize(Map);
	int DispEOIMenu();
	int DispEOSMenu();
	bool DispTrainHistory();
	bool DispSimMetrics();
	void CollectMetricsFromMap();
};