#pragma once
#include <list>



class Vertex
{
private:
	int id;
	int type;
	std::list<Track> tracks;
	std::list<HubTrack> hubTracks;

public:
	Vertex();
	~Vertex();
	int getID();
	int getType();
	std::list<Track> GetTracks();
	std::list <HubTrack> GetHubTracks();
	bool setType(int);
	bool addTrackConn(Track);
	bool addHubTrackConn(HubTrack);

};

