#pragma once
#include <string>
class Maintenance
{
private:
	std::string type;
	int id;

public:
	Maintenance(std::string myType, int myID) : type(myType), id(myID) {}
	std::string GetType();
	int GetID();
	void SetTypeString(std::string);
	void SetID();
};