#ifndef TRAIN_H_
#define TRAIN_H_

#include <list>

struct Stop;

class HubTrack;
class Vertex;

class Train
{
public:
	Train(HubTrack);
	bool GetOnTrack();
	void SetOnTrack();
	bool GetInHub();
	void SetInHub();
	int GetCrewTimeLeft();
	void SetCrewTimeLeft();
	int GetFuelLeft();
	void SetFuelLeft();
	int GetCurrentSpeed();
	void SetCurrentSpeed();
	int GetPosition();
	void SetPosition();
	int GetMaxCap();
	void SetMaxCap();
	int GetUsedCap();
	void SetUsedCap();
	void UpdateCap();
	std::list<Vertex> GetRoute();
	void SetRoute();
	HubTrack GetHomeHub();
	void SetHomeHub();

private:
	int trainId;
	std::string trackName;
	bool onTrack;
	int crewTimeLeft;
	int fuelLeft;
	int currentTrackId;
	int position;
	int currentSpeed;
	int crewUsed;
	int maxCap;
	int usedCap;
	std::list<Stop> route;

};//end Train

#endif // !TRAIN_H_
