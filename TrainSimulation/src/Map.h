#pragma once
#include "Track.h"
#include "Station.h"
#include "Hub.h"
#include "Hubtracks.h"
#include "Train.h"
#include "Vertex.h"
#include <list>

class Map
{
private:
	std::list<Tracks> MyTracks;
	std::list<Station> MyStations;
	std::list<Hub> MyHubs;
	std::list<Hubtracks> MyHubtracks;
	std::list<Train> MyTrains;

public:
	Map();
	bool AddTrack(int, Vertex, Vertex, int);
	bool AddVertex(int, int);
	bool AddHubTrack(int, Vertex, Vertex, int);
	bool AddTrain(int);
	void WeatherCalc();
};