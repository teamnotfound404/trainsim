#pragma once
#include "HubTrack.h"
#include "Track.h"
#include <list>



class Edge
{

private: 
	int length;
	Vertex endPoint1;
	Vertex endPoint2;
	int id;
	std::list<int> trackQueue;
	int weatherSeverity;
	bool active;
	int fuelCost;
	int totalTrainsCount;
	int highestQueueCount;

public:
	Edge();
	~Edge();
	int getLength();
	void setLength();
	Vertex getEndPoint1();
	Vertex getEndPoint2();
	void setEndPoint1();
	void setEndPoint2();
	void addToQueue();
	Train popFrontOfQueue(); //Train class needed; add #include "Train.h" when available
	bool getStatus();
	void clearQueue();
	void setWeatherSeverity();
	int getFuelCost();
	bool getActiveStatus();
	void setActiveStatus();

};

