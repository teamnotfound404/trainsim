//Missing other dependencies
//Created: 3/23/2019

#include <list>
#include "Train.h"
#include "HistoryLogger.h"
#include "HubTrack.h"
#include "Track.h"
#include "Vertex.h"

using namespace std;

struct Stop {
	Vertex station;
	int expectedTime;
	bool shouldStop;
	Track nextTrack;
};//end Stop

Train::Train(HubTrack hubInstance) {

};//end Train	

bool GetOnTrack() {

};//end GetOnTrack

void SetOnTrack() {

};//end SetOnTrack

bool GetInHub() {

};//end GetInHub

void SetInHub() {

};//end SetInHub
int GetCrewTimeLeft() {

};//end GetCrewTimeLeft

void SetCrewTimeLeft() {

};//end SetCrewTimeLeft

int GetFuelLeft() {

};//end GetFuelLeft

void SetFuelLeft() {

};//setFuelLeft

int GetCurrentSpeed() {

};//end GetCurrentSpeed

void SetCurrentSpeed() {

};//end SetCurrentSpeed

int GetPosition() {

};//end GetPosition

void SetPosition() {

};//end SetPosition

int GetMaxCap() {

};//end GetMaxCap

void SetMaxCap() {

};//end SetMaxCap

int GetUsedCap() {

};//end GetUsedCap

void SetUsedCap() {

};//end SetUsedCap

void UpdateCap() {

};//end UpdateCap

list<Vertex> GetRoute() {

};//end GetRoute

void SetRoute() {

};//end SetRoute

HubTrack GetHomeHub() {

};//end GetHomeHub

void SetHomeHub() {

};//end SetHomeHub

HubTrack homeHub;
HistoryLogger Logger;