#pragma once
#include "Vertex.h"

class Track
{
private:
	bool active;
	bool inUse;

public:
	Track();
	~Track();
	bool getActiveBoolStatus();
	void setActiveBoolStatus();
	bool getInUse();
	void setInUse();
};

