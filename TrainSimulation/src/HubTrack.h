#pragma once
#include "Vertex.h"

class HubTrack
{
private:
	int trainLimit;
	int currentNumberOfTrains;

public:
	HubTrack();
	~HubTrack();
	int getTrainLimit();
	void setTrainLimit();

};

