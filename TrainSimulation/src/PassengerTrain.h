#ifndef PASSENGERTRAIN_H
#define PASSENGERTRAIN_H

#include "HubTrack.h"
#include "Train.h"

class PassengerTrain {
	int ticketsSold;

public:
	PassengerTrain(HubTrack);
	int GetTrainSold();
	void SetTicketsSold();

};//end PassengerTrain

#endif // !PASSENGERTRAIN_H

